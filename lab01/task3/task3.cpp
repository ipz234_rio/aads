﻿#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

int main() {
    signed char a = 5;
    signed char b = 127;
    signed char c = 2;
    signed char d = 3;
    signed char e = -120;
    signed char f = -34;
    signed char g = -5;
    signed char h = 56;
    signed char i = 38;

    signed char result_a = a + b;               // Операція а)
    signed char result_b = c - d;               // Операція б)
    signed char result_c = e - f;               // Операція в)
    unsigned char result_d = (unsigned char)g;  // Операція г)
    signed char result_e = h & i;               // Операція д)
    signed char result_f = h | i;               // Операція е)

    printf("a) %d + %d = %d\n", a, b, result_a);
    printf("b) %d - %d = %d\n", c, d, result_b);
    printf("c) %d - %d = %d\n", e, f, result_c);
    printf("d) (unsigned char) (%d) = %u\n", g, result_d);
    printf("e) %d & %d = %d\n", h, i, result_e);
    printf("f) %d | %d = %d\n", h, i, result_f);

    return 0;
}
