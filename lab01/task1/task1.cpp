﻿#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <time.h>

typedef struct {
	unsigned short hour;
	unsigned short minute;
	unsigned short second;
} Time;

typedef struct {
	Time time;
	unsigned short year;
	unsigned short month;
	unsigned short day;
} Date;

typedef struct {
	unsigned short hour : 5;
	unsigned short minute : 6;
	unsigned short second : 6;
} nTime;

typedef struct {
	nTime time;
	unsigned short year : 11;
	unsigned short month : 5;
	unsigned short day : 6;
} nDate;



int main() {

	nDate compactTime;
	Date noCompactTime;

	time_t updateTime;
	struct tm* infoTime;
	time(&updateTime);
	infoTime = localtime(&updateTime);

	compactTime.year = infoTime->tm_year + 1900;
	compactTime.month = infoTime->tm_mon + 1;
	compactTime.day = infoTime->tm_mday;
	compactTime.time.hour = infoTime->tm_hour;
	compactTime.time.minute = infoTime->tm_min;
	compactTime.time.second = infoTime->tm_sec;

	printf("compactTime:\n");
	printf("%hu:%hu:%hu\n%hu.%hu.%hu\n", compactTime.time.hour, compactTime.time.minute, compactTime.time.second, compactTime.day, compactTime.month, compactTime.year);
	printf("sizeof - %zu\n\n", sizeof(compactTime));

	noCompactTime.year = infoTime->tm_year + 1900;
	noCompactTime.month = infoTime->tm_mon + 1;
	noCompactTime.day = infoTime->tm_mday;
	noCompactTime.time.hour = infoTime->tm_hour;
	noCompactTime.time.minute = infoTime->tm_min;
	noCompactTime.time.second = infoTime->tm_sec;

	printf("noCompactTime:\n");
	printf("%hu:%hu:%hu\n%hu.%hu.%hu\n", noCompactTime.time.hour, noCompactTime.time.minute, noCompactTime.time.second, noCompactTime.day, noCompactTime.month, noCompactTime.year);
	printf("sizeof - %zu\n", sizeof(noCompactTime));
}
