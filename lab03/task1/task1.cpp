﻿#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <chrono>
#include <iostream>
#include <windows.h>

#define GETTIME std::chrono::steady_clock::now
#define CALCTIME std::chrono::duration_cast<std::chrono::nanoseconds>

double func1(double n) {
	return n;
}

double func2(double n) {
	return log(n);
}

double func3(double n) {
	return n * log(n);
}

double func4(double n) {
	return n * n;
}

double func5(double n) {
	double result = 2;
	for (int i = 1; i <= n; i++) {
		result *= n;
	}
	return result;
}

double func6(double n) {
	if (n == 0) {
		return 1;
	}
	else {
		return n * func6(n - 1);
	}
}

char* funcZad9(int a, int b) {
	char* binaryString = (char*)malloc(b + 1);
	if (binaryString == NULL) {
		printf("Memory allocation failed.\n");
		exit(1);
	}

	for (int i = b - 1; i >= 0; i--) {
		binaryString[i] = (a & 1) ? '1' : '0';
		a >>= 1;
	}
	binaryString[b] = '\0';

	return binaryString;
}

void funcZad5(int octalDigits[], int n) {
	for (int i = 0; i < n - 1; i++) {
		for (int j = 0; j < n - i - 1; j++) {
			if (octalDigits[j] < octalDigits[j + 1]) {
				int temp = octalDigits[j];
				octalDigits[j] = octalDigits[j + 1];
				octalDigits[j + 1] = temp;
			}
		}
	}

	for (int i = 0; i < n; i++) {
		printf("%d ", octalDigits[i]);
	}
}


int main() {

	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	auto begin = GETTIME();
	auto finish = GETTIME();
	auto elapsed_ns = CALCTIME(finish - begin).count();

	for (int i = 0; i <= 50; i++) {
		auto begin = GETTIME();

		printf("[%d] [f(n) = n] %.2f | ", i, func1(i));
		auto finish = GETTIME();
		auto elapsed_ns = CALCTIME(finish - begin).count();
		printf("The time: %lld ms\n\n", elapsed_ns);
	}

	for (int i = 0; i <= 50; i++) {
		begin = GETTIME();
		printf("[%d] [f(n) = log(n)] %.2f | ", i, func2(i));
		finish = GETTIME();
		elapsed_ns = CALCTIME(finish - begin).count();
		printf("The time: %lld ms\n\n", elapsed_ns);
	}

	for (int i = 0; i <= 50; i++) {
		begin = GETTIME();
		printf("[%d] [f(n) = n * log(n)] %.2f | ", i, func3(i));
		finish = GETTIME();
		elapsed_ns = CALCTIME(finish - begin).count();
		printf("The time: %lld ms\n\n", elapsed_ns);
	}

	for (int i = 0; i <= 50; i++) {
		begin = GETTIME();
		printf("[%d] [f(n) = n^2] %.2f | ", i, func4(i));
		finish = GETTIME();
		elapsed_ns = CALCTIME(finish - begin).count();
		printf("The time: %lld ms\n\n", elapsed_ns);
	}

	for (int i = 0; i <= 50; i++) {
		begin = GETTIME();

		printf("[%d] [f(n) = 2^n] %.2f | ", i, func5(i));
		finish = GETTIME();
		elapsed_ns = CALCTIME(finish - begin).count();
		printf("The time: %lld ms\n\n", elapsed_ns);
	}

	for (int i = 0; i <= 50; i++) {
		begin = GETTIME();
		printf("[%d] [f(n) = n!] %.2f | ", i, func6(i));
		finish = GETTIME();
		elapsed_ns = CALCTIME(finish - begin).count();
		printf("The time: %lld ms\n\n", elapsed_ns);
	}
	
	for (int i = 0; i <= 50; i++) {
		int a = rand() % 11;
		int b = rand() % (64 - 8 + 1) + 8;
		begin = GETTIME();
		char* binaryRepresentation = funcZad9(a, b);
		finish = GETTIME();
		elapsed_ns = CALCTIME(finish - begin).count();
		printf("Binary representation of %d(%d) is: %s | time - %lld ms\n", a, b, binaryRepresentation, elapsed_ns);
	}

	int octalDigits[20];
	for (int i = 0; i <= 50; i++) {
		printf("\nArray of octal digits: ");
		for (int i = 0; i < 20; i++) {
			octalDigits[i] = rand() % 8; 
			printf("%d ", octalDigits[i]);
		}
		printf("| Sorted ");

		begin = GETTIME();
		funcZad5(octalDigits, 20); 
		finish = GETTIME();
		elapsed_ns = CALCTIME(finish - begin).count();

		printf("| time - %lld ms", elapsed_ns);
	}

}