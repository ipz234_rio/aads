﻿#include <iostream>
#include <cmath>     // Для std::pow і std::floor
#include <vector>    // Для зберігання інтервалів
#include <chrono>

void shellSort(std::vector<double>& arr) {
    int n = arr.size();
    // Створюємо вектор для зберігання приростів
    std::vector<int> sedgewick;
    int h = 1;
    // Генеруємо прирости доки вони менші за розмір масиву
    while (h < n) {
        sedgewick.push_back(h);
        h = 3 * h + 1; // Наступний приріст за формулою Р.Седжвіка
    }

    // Проходимося по приростах у зворотньому порядку
    for (int i = sedgewick.size() - 1; i >= 0; i--) {
        h = sedgewick[i];
        // Сортування вставками для поточного приросту
        for (int j = h; j < n; j++) {
            double temp = arr[j];
            int k;
            for (k = j; k >= h && arr[k - h] > temp; k -= h) {
                arr[k] = arr[k - h];
            }
            arr[k] = temp;
        }
    }
}

// Функція для генерації випадкових double чисел у заданому діапазоні
void generateRandomDoubles(std::vector<double>& arr) {
    for (double& val : arr) {
        val = static_cast<double>(rand() % 321 - 20); // Генерація випадкового числа в діапазоні від -20 до 300
    }
}

int main() {
    srand(time(NULL));

    int sizes[] = { 10, 100, 500, 1000, 2000, 5000, 10000 }; // Розміри масивів
    int numSizes = sizeof(sizes) / sizeof(sizes[0]); // Кількість розмірів

    // Проходимо по кожному розміру масиву
    for (int i = 0; i < numSizes; i++) {
        int size = sizes[i]; // Поточний розмір масиву
        std::vector<double> data(size); // Створюємо масив заданого розміру

        // Генеруємо випадкові дані для масиву
        generateRandomDoubles(data);

        // Вимірюємо час сортування
        auto start = std::chrono::high_resolution_clock::now(); // Початок вимірювання часу
        shellSort(data); // Сортуємо масив
        auto end = std::chrono::high_resolution_clock::now(); // Кінець вимірювання часу

        // Обчислюємо тривалість виконання та виводимо результат
        std::chrono::duration<double, std::milli> duration = end - start; // Тривалість в мілісекундах
        std::cout << "Sorting " << size << " elements took: " << duration.count() << " milliseconds." << std::endl;
    }

    return 0;
}
