﻿#include <iostream>
#include <vector>    // Для зберігання масиву та інтервалів
#include <chrono>    // Для функцій часу
#include <cstdlib>   // Для rand() та srand()
#include <ctime>     // Для time()

void shellSort(std::vector<double>& arr) {
    int n = arr.size();
    // Створюємо вектор для зберігання приростів
    std::vector<int> sedgewick;
    int h = 1;
    // Генеруємо прирости доки вони менші за розмір масиву
    while (h < n) {
        sedgewick.push_back(h);
        h = 3 * h + 1; // Наступний приріст за формулою Р.Седжвіка
    }

    // Проходимося по приростах у зворотньому порядку
    for (int i = sedgewick.size() - 1; i >= 0; i--) {
        h = sedgewick[i];
        // Сортування вставками для поточного приросту
        for (int j = h; j < n; j++) {
            double temp = arr[j];
            int k;
            for (k = j; k >= h && arr[k - h] > temp; k -= h) {
                arr[k] = arr[k - h];
            }
            arr[k] = temp;
        }
    }
}

// Функція для генерації випадкових double чисел у заданому діапазоні
void generateRandomDoubles(std::vector<double>& arr) {
    for (double& val : arr) {
        val = static_cast<double>(rand() % 321 - 20); // Генерація випадкового числа в діапазоні від -20 до 300
    }
}

int main() {
    srand(static_cast<unsigned>(time(nullptr)));

    int size = 10; // Розміри масиву
    std::vector<double> data(size); // Створюємо вектор заданого розміру

    generateRandomDoubles(data); // Генеруємо випадкові дані для вектору
    std::cout << "Original array:\n";
    for (double el : data) {
        std::cout << el << " ";
    }
    std::cout << "\n";

    shellSort(data); // Сортуємо вектор
    std::cout << "Sorted array:\n";
    for (double el : data) {
        std::cout << el << " ";
    }
    std::cout << "\n";

    return 0;
}