﻿#include <iostream>
#include <vector>
#include <chrono>

void countingSort(short arr[], int n) {
    int range = 211; // Розмір діапазону (-200 до 10)
    int offset = 200; // Зсув для позитивних індексів
    std::vector<int> count(range, 0); // Вектор для підрахунку

    for (int i = 0; i < n; i++) {
        count[arr[i] + offset]++; // Збільшуємо лічильник відповідної позиції
    }

    int index = 0; // Індекс для вставки елементів назад в масив
    for (int i = 0; i < range; i++) {
        while (count[i] > 0) {
            arr[index++] = i - offset; // Вставка і зменшення лічильника
            count[i]--;
        }
    }
}


// Функція для генерації випадкових short чисел у заданому діапазоні
void generateRandomShorts(short arr[], int n) {
    for (int i = 0; i < n; i++) {
        arr[i] = rand() % 211 - 200; // Генерація чисел від -200 до 10
    }
}

int main() {
    srand(time(NULL));

    int size = 10; // Розміри масивів
    short* data = new short[size]; // Створюємо масив заданого розміру
        
    generateRandomShorts(data, size); // Генеруємо випадкові дані для масиву
    std::cout << "No sorted array:\n";
    for (int i = 0; i < size; i++) {
        std::cout << data[i] << " ";
    }
    std::cout << std::endl;

    countingSort(data, size); // Сортуємо масив
    std::cout << "\nSorted array:\n";
    for (int i = 0; i < size; i++) {
        std::cout << data[i] << " ";
    }
    std::cout << std::endl;





    return 0;
}
