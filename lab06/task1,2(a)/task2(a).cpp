﻿#include <iostream>
#include <algorithm> // Для std::swap
#include <ctime>     // Для time
#include <cstdlib>   // Для rand, srand
#include <chrono>    // Для виміру часу

void heapify(int arr[], int n, int i) {
    int largest = i; // Ініціалізація найбільшого елементу як кореня
    int left = 2 * i + 1; // лівий = 2*i + 1
    int right = 2 * i + 2; // правий = 2*i + 2

    // Якщо лівий дочірній елемент більший за корінь
    if (left < n && arr[left] > arr[largest])
        largest = left;

    // Якщо правий дочірній елемент більший, ніж найбільший до цього
    if (right < n && arr[right] > arr[largest])
        largest = right;

    // Якщо найбільший не є коренем
    if (largest != i) {
        std::swap(arr[i], arr[largest]);
        // Рекурсивно просіюємо впливовий піддерево
        heapify(arr, n, largest);
    }
}

// основна функція для сортування масиву даних типу float
void heapSort(int arr[], int n) {
    // Побудова купи (перегрупувати масив)
    for (int i = n / 2 - 1; i >= 0; i--)
        heapify(arr, n, i);

    // Один за одним виймати елементи з купи
    for (int i = n - 1; i >= 0; i--) {
        std::swap(arr[0], arr[i]); // Поточний корінь переміщуємо в кінець
        heapify(arr, i, 0); // викликаємо процес просіювання на зменшеному купі
    }
}

// Функція для генерації випадкових float чисел у заданому діапазоні
void generateRandomInt(int arr[], int n) {
    for (int i = 0; i < n; i++) {
        arr[i] = rand() % 101; // Генерація випадкового числа в діапазоні від 0 до 100
    }
}

int main() {
    srand(time(NULL));

    int sizes[] = { 10, 100, 500, 1000, 2000, 5000, 10000 }; // Розміри масивів
    int numSizes = sizeof(sizes) / sizeof(sizes[0]); // Кількість розмірів

    // Проходимо по кожному розміру масиву
    for (int i = 0; i < numSizes; i++) {
        int size = sizes[i]; // Поточний розмір масиву
        int* data = new int[size]; // Створюємо масив заданого розміру

        // Генеруємо випадкові дані для масиву
        generateRandomInt(data, size);

        // Вимірюємо час сортування
        auto start = std::chrono::high_resolution_clock::now(); // Початок вимірювання часу
        heapSort(data, size); // Сортуємо масив
        auto end = std::chrono::high_resolution_clock::now(); // Кінець вимірювання часу

        // Обчислюємо тривалість виконання та виводимо результат
        std::chrono::duration<double, std::milli> duration = end - start; // Тривалість в мілісекундах
        std::cout << "Sorting " << size << " elements took: " << duration.count() << " milliseconds." << std::endl;

        delete[] data; // Звільняємо пам'ять, щоб уникнути витоку
    }


    return 0;
}
