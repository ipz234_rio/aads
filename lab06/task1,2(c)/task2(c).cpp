﻿#include <iostream>
#include <vector>
#include <chrono>

void countingSort(short arr[], int n) {
    int range = 211; // Розмір діапазону (-200 до 10)
    int offset = 200; // Зсув для позитивних індексів
    std::vector<int> count(range, 0); // Вектор для підрахунку

    for (int i = 0; i < n; i++) {
        count[arr[i] + offset]++; // Збільшуємо лічильник відповідної позиції
    }

    int index = 0; // Індекс для вставки елементів назад в масив
    for (int i = 0; i < range; i++) {
        while (count[i] > 0) {
            arr[index++] = i - offset; // Вставка і зменшення лічильника
            count[i]--;
        }
    }
}


// Функція для генерації випадкових short чисел у заданому діапазоні
void generateRandomShorts(short arr[], int n) {
    for (int i = 0; i < n; i++) {
        arr[i] = rand() % 211 - 200; // Генерація чисел від -200 до 10
    }
}

int main() {
    srand(time(NULL));

    int sizes[] = { 10, 100, 500, 1000, 2000, 5000, 10000 }; // Розміри масивів
    int numSizes = sizeof(sizes) / sizeof(sizes[0]); // Кількість розмірів

    // Проходимо по кожному розміру масиву
    for (int i = 0; i < numSizes; i++) {
        int size = sizes[i]; // Поточний розмір масиву
        short* data = new short[size]; // Створюємо масив заданого розміру

        // Генеруємо випадкові дані для масиву
        generateRandomShorts(data, size);

        // Вимірюємо час сортування
        auto start = std::chrono::high_resolution_clock::now(); // Початок вимірювання часу
        countingSort(data, size); // Сортуємо масив
        auto end = std::chrono::high_resolution_clock::now(); // Кінець вимірювання часу

        // Обчислюємо тривалість виконання та виводимо результат
        std::chrono::duration<double, std::milli> duration = end - start; // Тривалість в мілісекундах
        std::cout << "Sorting " << size << " elements took: " << duration.count() << " milliseconds." << std::endl;

    }

    return 0;
}
