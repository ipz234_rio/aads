﻿#include <stdio.h>
#include <stdlib.h>

typedef struct Node {
    int data;           // Дані, що містяться в вузлі
    struct Node* next;  // Вказівник на наступний вузол
    struct Node* prev;  // Вказівник на попередній вузол
} Node;

Node* createNode(int data) {
    Node* newNode = (Node*)malloc(sizeof(Node));
    newNode->data = data;
    newNode->next = NULL;
    newNode->prev = NULL;
    return newNode;  // Повертаємо вказівник на новий вузол
}

void appendNode(Node** head, int data) {
    Node* newNode = createNode(data);
    if (*head == NULL) {
        *head = newNode;
        return;
    }   
    Node* temp = *head;
    while (temp->next != NULL) {
        temp = temp->next;
    }
    temp->next = newNode;
    newNode->prev = temp;
}

void selectionSort(Node* head) {
    Node* temp, * min;
    int tempData;

    for (temp = head; temp != NULL; temp = temp->next) {
        min = temp;
        for (Node* r = temp->next; r != NULL; r = r->next) {
            if (r->data < min->data) {
                min = r;
            }
        }
        if (min != temp) {
            tempData = temp->data;
            temp->data = min->data;
            min->data = tempData;
        }
    }
}

void printList(Node* head) {
    Node* temp = head;
    while (temp != NULL) {
        printf("%d ", temp->data);
        temp = temp->next;
    }
    printf("\n");
}

int main() {
    Node* head = NULL;
    appendNode(&head, 3);
    appendNode(&head, 1);
    appendNode(&head, 4);
    appendNode(&head, 2);

    printf("Before sorting: ");
    printList(head);

    selectionSort(head);

    printf("After sorting: ");
    printList(head);

    return 0;
}
