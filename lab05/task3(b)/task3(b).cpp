﻿#include <iostream>
#include <chrono>
#include <ctime> 
#include <stdio.h>
#include <stdlib.h>

void insertionSort(int arr[], int n) {
    for (int i = 1; i < n; i++) {
        int key = arr[i];
        int j = i - 1;

        while (j >= 0 && arr[j] > key) {
            arr[j + 1] = arr[j];
            j = j - 1;
        }
        arr[j + 1] = key;
    }
}

void printArray(int arr[], int n) {
    for (int i = 0; i < n; i++) {
        printf("%d ", arr[i]);
    }
    printf("\n");
}

void generateArray(int arr[], int n) {
    srand(time(NULL));
    for (int i = 0; i < n; i++) {
        int randNumber = rand() % 22;
        arr[i] = randNumber;
    }
}

int main() {
    int sizes[] = { 10, 100, 500, 1000, 2000, 5000, 10000 }; // Розміри масивів
    int numSizes = sizeof(sizes) / sizeof(sizes[0]); // Кількість розмірів
    int arr[10000];

    for (int i = 0; i < numSizes; i++) {
        int size = sizes[i]; // Поточний розмір масиву


        generateArray(arr, size);

        auto start = std::chrono::high_resolution_clock::now(); // Початок вимірювання часу
        insertionSort(arr, size);
        auto end = std::chrono::high_resolution_clock::now(); // Кінець вимірювання часу

        std::chrono::duration<double, std::milli> duration = end - start; // Тривалість в мілісекундах
        std::cout << "Sorting " << size << " elements took: " << duration.count() << " milliseconds." << std::endl;

    }

    return 0;
}
