﻿#include <iostream>
#include <chrono>
#include <ctime> 
#include <stdio.h>
#include <stdlib.h>

typedef struct Node {
    int data;           // Дані, що містяться в вузлі
    struct Node* next;  // Вказівник на наступний вузол
    struct Node* prev;  // Вказівник на попередній вузол
} Node;

Node* createNode(int data) {
    Node* newNode = (Node*)malloc(sizeof(Node));
    newNode->data = data;
    newNode->next = NULL;
    newNode->prev = NULL;
    return newNode;  // Повертаємо вказівник на новий вузол
}

void appendNode(Node** head, int data) {
    Node* newNode = createNode(data);
    if (*head == NULL) {
        *head = newNode;
        return;
    }
    Node* temp = *head;
    while (temp->next != NULL) {
        temp = temp->next;
    }
    temp->next = newNode;
    newNode->prev = temp;
}

void selectionSort(Node* head) {
    Node* temp, * min;
    int tempData;

    for (temp = head; temp != NULL; temp = temp->next) {
        min = temp;
        for (Node* r = temp->next; r != NULL; r = r->next) {
            if (r->data < min->data) {
                min = r;
            }
        }
        if (min != temp) {
            tempData = temp->data;
            temp->data = min->data;
            min->data = tempData;
        }
    }
}

void printList(Node* head) {
    Node* temp = head;
    while (temp != NULL) {
        printf("%d ", temp->data);
        temp = temp->next;
    }
    printf("\n");
}

void generateNode(Node** head, int count) {
    srand(time(NULL));

    for (int i = 1; i <= count; i++) {
        int randNumber = rand() % 23;
        appendNode(head, randNumber);
    }
}

int main() {
    int sizes[] = { 10, 100, 500, 1000, 2000, 5000, 10000 }; // Розміри масивів
    int numSizes = sizeof(sizes) / sizeof(sizes[0]); // Кількість розмірів
    Node* head = NULL;

    for (int i = 0; i < numSizes; i++) {
        int size = sizes[i]; // Поточний розмір масиву


        generateNode(&head, size);
        //printList(head);

        auto start = std::chrono::high_resolution_clock::now(); // Початок вимірювання часу
        selectionSort(head); // Сортуємо масив
        auto end = std::chrono::high_resolution_clock::now(); // Кінець вимірювання часу

        std::chrono::duration<double, std::milli> duration = end - start; // Тривалість в мілісекундах
        std::cout << "Sorting " << size << " elements took: " << duration.count() << " milliseconds." << std::endl;

        // Звільнення пам'яті
        while (head != NULL) {
            Node* temp = head;
            head = head->next;
            free(temp);
        }
    }

    return 0;
}
